# Dispatcher Config Sublime Text package #

A Sublime Text package with simple syntax highlighting and code navigation support for Dispatcher configuration files

## Features ##

* Syntax highlighting, including configuration blocks, comments, variables and strings
* Jump between significant symbols (e.g. using Cmd/Ctrl+R)
* Automatic detection of .any files

## Installation using Package Control ##

First, [install Package Control](https://packagecontrol.io/installation) if you haven't already. Then, add this repository to Package Control:

1. Open the command menu (Cmd/Ctrl+Shift+P)
1. Select "Package Control: Add Repository"
1. Enter the URL of this repository (i.e https://bitbucket.org/djessup/dispatcher-config-sublime-text)

Finally, install the package using Package Control:

1. Open the command menu
1. Search for "Dispatcher Config"
1. Install the package

The package will be installed and automatically kept up-to-date by Package Control.

## Manual installation ##

1. Click the Preferences > Browse Packages… menu
1. Browse up a folder and then into the Installed Packages/ folder
1. Download Dispatcher Config.sublime-package from the Downloads section of this repository and copy it into the Installed Packages/ directory
1. Restart Sublime Text